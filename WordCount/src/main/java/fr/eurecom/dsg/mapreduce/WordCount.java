package fr.eurecom.dsg.mapreduce;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;

/**
 * Word Count example of MapReduce job. Given a plain text in input, this job
 * counts how many occurrences of each word there are in that text and writes
 * the result on HDFS.
 *
 */
public class WordCount extends Configured implements Tool {

    private int numReducers;
    private Path inputPath;
    private Path outputDir;

    @Override
    public int run(String[] args) throws Exception {

        Configuration conf = this.getConf();

        //  define new job instead of null using conf
        Job job = new Job(conf, "wordcount");


        //  set job input format
        job.setInputFormatClass(TextInputFormat.class);

        //  set map class and the map output key and value classes
        job.setMapperClass(WCMapper.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        //  set reduce class and the reduce output key and value classes
        job.setReducerClass(WCReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        //  set job output format
        job.setOutputFormatClass(TextOutputFormat.class);

        //  add the input file as job input (from HDFS) to the variable
        //       inputPath
        FileInputFormat.addInputPath(job,inputPath);

        //  set the output path for the job results (to HDFS) to the variable
        //       outputPath
        FileOutputFormat.setOutputPath(job,outputDir);

        //  set the number of reducers using variable numberReducers
        job.setNumReduceTasks(numReducers);

        //  set the jar class
        job.setJarByClass(WordCount.class);

        return job.waitForCompletion(true) ? 0 : 1; // this will execute the job
    }

    public WordCount (String[] args) {
        if (args.length != 3) {
            System.out.println("Usage: WordCount <num_reducers> <input_path> <output_path>");
            System.exit(0);
        }
        this.numReducers = Integer.parseInt(args[0]);
        this.inputPath = new Path(args[1]);
        this.outputDir = new Path(args[2]);
    }

    public static void main(String args[]) throws Exception {
        int res = ToolRunner.run(new Configuration(), new WordCount(args), args);
        System.exit(res);
    }
}

class WCMapper extends Mapper<Text, //  change Object to input key type
        Text, //  change Object to input value type
        Text, //  change Object to output key type
        Text> { //  change Object to output value type

    @Override
    protected void map(Text key, //  change Object to input key type
                       Text value, //  change Object to input value type
                       Context context) throws IOException, InterruptedException {

        //  implement the map method (use context.write to emit results)
        StringTokenizer st = new StringTokenizer(value.toString(),",");
        while (st.hasMoreTokens())
        {
            context.write(new Text(st.nextToken()),new Text("1"));
        }

    }

}

class WCReducer extends Reducer<Text, //  change Object to input key type
        Text, // change Object to input value type
        Text, // change Object to output key type
        Text> { // change Object to output value type

    @Override
    protected void reduce(Text key, //  change Object to input key type
                          Iterable<Text> values, // change Object to input value type
                          Context context) throws IOException, InterruptedException {

        //  implement the reduce method (use context.write to emit results)

        Integer count = 0;

        for (Text val : values) {
            count += Integer.parseInt(val.toString());
        }

        context.write(key, new Text(count.toString()));
    }
}
